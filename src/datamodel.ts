type Track = {
  title: string;
  artists: string[];
  album: { albumtitle: string };
};

type MostPlayed = {
  scrobbles: number;
  id: number;
  rank: number;
};

export type MostPlayedAlbum = MostPlayed & {
  album: { albumtitle: string; artists: string[] };
};

export type MostPlayedArtist = MostPlayed & { artist: string };

export type MostPlayedTrack = MostPlayed & { track: Track };

export type LastPlayedResponse = {
  data: { scrobbles: { recentSongs: { time: number; track: Track }[] } };
};

export type TopAlbumsResponse = {
  data: { scrobbles: { mostPlayedAlbums: MostPlayedAlbum[] } };
};

export type TopArtistsResponse = {
  data: { scrobbles: { mostPlayedArtists: MostPlayedArtist[] } };
};

export type TopTracksResponse = {
  data: { scrobbles: { mostPlayedTracks: MostPlayedTrack[] } };
};

type BlogPhoto = {
  photo: string;
  title: string;
  id: string;
  url: string;
};

export type PhotoResponse = {
  data: {
    photoBlog: { photos: BlogPhoto[] };
  };
};
